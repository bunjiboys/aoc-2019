package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"math"
	"os"
	"strconv"
	"strings"
)

func calculateTotalFuel(mass int64) int64 {
	fuel := calculateFuel(mass)
	totalFuel := fuel

	for fuel >= 0 {
		fuelCost := calculateFuel(fuel)
		if fuelCost <= 0 {
			break
		}
		totalFuel += fuelCost
		fuel = fuelCost
	}

	return totalFuel
}

func calculateFuel(mass int64) int64 {
	return int64(math.Floor(float64(mass/3)) - 2)
}

func check(e error) {
	if e != nil {
		log.Fatal(e)
	}
}

func loadModules(path string) []int64 {
	var modules []int64
	fh, err := os.Open(path)
	check(err)
	defer fh.Close()

	scanner := bufio.NewScanner(fh)
	for scanner.Scan() {
		moduleString := strings.TrimSpace(scanner.Text())
		check(err)
		if strings.HasPrefix(moduleString, "#") {
			continue
		}

		module, err := strconv.ParseInt(moduleString, 10, 64)
		check(err)
		modules = append(modules, module)
	}
	check(scanner.Err())

	return modules
}

func main() {
	inputFile := flag.String("modules", "", "Modules file to load")
	flag.Parse()

	// Alert if no input file was provided
	if *inputFile == "" {
		log.Fatal("You must provide the -modules <path> argument")
	}

	// Read in the modules file
	launchModules := loadModules(*inputFile)
	var fuelRequired int64 = 0

	// Calculate the total fuel cost of all modules
	for i := 0; i < len(launchModules); i++ {
		fuelRequired += calculateTotalFuel(launchModules[i])
	}

	fmt.Println("Fuel required to launch all modules are:", fuelRequired)
}
